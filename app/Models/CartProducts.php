<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartProducts extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'cart_products';
    protected $fillable = [
        'cart_id',
        'product_id',
        'qty'
    ];

    public function product() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function Cart() {
        return $this->hasOne('App\Models\Cart', 'id', 'cart_id');
    }


}

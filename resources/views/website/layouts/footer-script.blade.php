

        @yield('script')

        <script src="{{ URL::asset('assets/js/website/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/website/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/website/popper.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/website/slick.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/website/bootstrap-slider.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/website/custom.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/lottiefiles/lottie-player.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                    $('form').parsley();
                    $('.select2').select2();
                    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                        $(".alert").slideUp(500);
                    });

                    $('#datepicker-autoclose').datepicker({
                        format: 'mm-dd-yyyy',
                        endDate: '-21y',
                        autoclose: true
                    });


            });
        </script>
        <div id="loader" style="width: 100%; height: 100%; position: fixed;display: block;top: 0;left: 0;text-align: center;opacity: 1;background-color: #ffffff73;z-index: 111111; display: none;">
            <lottie-player src="{{ URL::asset('assets/libs/lottiefiles/lf20_i2iugofy.json')}}" background="transparent" speed="1" style="width: 250px; height: 250px; position: absolute;top: 36%;left: 46%;z-index: 1111;" autoplay loop></lottie-player>
       </div>
